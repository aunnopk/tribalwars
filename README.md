# tribalwars-open-source

## About

These files were downloaded from:
- [http://forum.ragezone.com/f857/release-tribalwars-lan-opensource-100-a-1006737/](http://forum.ragezone.com/f857/release-tribalwars-lan-opensource-100-a-1006737/)

All credit and ownership belong to whoever hosted (or owns) these files.

Some files were translated to English. Additional files were added outside of this package to make
building the project easier, including but not limited to Docker files and launch scripts.

## Requirements

You'll need to install `Docker` and `docker-compose` to build this project.

## Build

### Using `docker-compose`

```bash
git clone https://gitlab.com/tribalwars/tribalwars.git
cd tribalwars/
chmod +x launch.sh
./launch.sh
```
