<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Vacation Replacement</title>
<link rel="stylesheet" type="text/css" href="stamm.css" />
<script src="script.js?1176997364" type="text/javascript"></script>
</head>

<body >
<table class="main" width="100%" align="center"><tr><td><h2>Urlaubsvertretung aktiv</h2>
<p>Time is <b>{$vacation_name}</b> your holiday representation. You can only return to your account when you finish the vacation.</p>

<div align="center">
	<a href="game.php?&amp;screen=&amp;action=end&amp;h={$hkey}">Finish vacation replacement</a>
</div>

<p><a href="game.php?&amp;screen=&amp;action=logout&amp;h={$hkey}">Logout</a></p>